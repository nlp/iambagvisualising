dash>=2.7.0
matplotlib>=3.6.2
plotly>=5.9.0
iambagging>=0.2.5
