#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Construct some plotly graph associated to the display of bars.
"""
from typing import Sequence

import plotly.graph_objects as go


def error_plot(x: Sequence[float], 
               y: Sequence[float],
               error: Sequence[float] = None,
               ) -> go.Figure:
    """Construct a simple scatter plot from x and y values (two sequences of same lengths), with eventually some error bars attached.
    
    Parameters
    ----------
    
    x, y: sequences of floats
        The x and y values of the bars.
    error: Sequence, optional
        Sequence of error values along the y-axis.

    Returns
    -------
    fig : plotly.graph_objects.Figure object
        The plotly figure, that can be further updated or displayed.    
    """
    if len(y) != len(x):
        raise ValueError("all sequences must have same length")
    if error is not None:
        if len(y) != len(error) or len(x) != len(error):
            raise ValueError("all sequences must have same length")
    if error is None:
        error_ = [0 for _ in y]
    fig = go.Figure(
        data=go.Scatter(
            x=x,
            y=y,
            error_y=dict(
                type='data',
                array=error if error is not None else error_,
                visible=True if error is not None else False,)
        )
    )
    return fig
