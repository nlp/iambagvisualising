#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Construction and save of a random BagOfWords.
Inspired by the script of iamBagging library.
"""

from random import choice, choices

from iambagging import BagOfWords
from iambagging import __version__

print(f"iambagging version {__version__}")
folder_name = '../test/'

# generate a list of words
words = ['word'+str(i) for i in range(10)]
# take a random length of document from n_min to n_max
n_min, n_max = 4, 15
len_choice = [choice(range(n_min,n_max)) for _ in range(12)]
# take randomly a word in words up to size given in len_choice
test_catalog = [[choice(words) for _ in range(lc)] for lc in len_choice]


def random_catalog(size_vocab=10,
                   size_documents=list(),
                   size_catalog=100,
                   weights_vocab=list()):
    if not list(size_documents):
        size_documents = [1, size_vocab]
    if not list(weights_vocab) or len(list(weights_vocab)) != int(size_vocab):
        weights_vocab = None
    tokens = ['w' + str(i) for i in range(int(size_vocab))]
    catalog = [choices(tokens,
                       k=choice(range(*size_documents)),
                       weights=weights_vocab)
               for _ in range(size_catalog)]
    
    return catalog


size_vocab = 150
weights_vocab = [5,5,5,5,5,20,2,2,2,2] + [1]*(size_vocab-10)
size_documents = [25,155]
size_catalog = 100
rand_cat = random_catalog(weights_vocab=weights_vocab,
                          size_documents=size_documents,
                          size_catalog=size_catalog,
                          size_vocab=size_vocab)

bow = BagOfWords(verbose=False)
bow.fit(rand_cat)

file_name = 'random_bow.npz'
bow.save(folder_name + file_name)
