#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Multivariate construction of figure using plotly.
"""
from typing import List

import numpy as np
import pandas as pd
import plotly.graph_objects as go

from .colors import colorscale


def radar_chart(df: pd.DataFrame, legend_title: str = None) -> go.Figure:
    """Take a pandas DataFrame, and constructs a radar chart.

    The DataFrame has angular-datas in columns, and the different curves will be given in rows. So for a single radar chart with n options the DataFrame will have the shape (1,n) for instance. Data must be inside the [0,1] range.

    Returns a plotly.Figure object.
    """
    categories = list(df.columns)

    fig = go.Figure()

    for name, data in df.iterrows():
        radii = [data[c] for c in categories]
        name = name
        fig.add_trace(go.Scatterpolar(
            r=radii + radii[:1],
            theta=categories + categories[:1],
            name=name,
        ))

    showlegend = False if legend_title is None else True

    fig.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=True,
                range=[0, 1]
            )
        ),
        legend_title=legend_title,
        showlegend=showlegend,
    )
    return fig


def parallel_coordinates(df: pd.DataFrame,
                         color_scale: List[float] = None,
                         legend_title: str = None) -> go.Figure:
    categories = list(df.columns)
    if colorscale is not None:
        color_scale = color_scale
    else:
        color_scale = list(colorscale(list(df.columns)).values())

    dimensions = []
    for name, data in df.iterrows():
        values = [data[c] for c in categories]
        dimensions.append(dict(values=values,
                               range=[min(values), max(values)],
                               label=name,))

    fig = go.Figure(data=go.Parcoords(
        dimensions=dimensions,
        line=dict(color=color_scale,
                  colorscale="viridis",
                  showscale=False,
                  cmin=0,
                  cmax=1.0),
        )
    )

    showlegend = False if legend_title is None else True

    fig.update_layout(
        legend_title=legend_title,
        showlegend=showlegend,
    )
    return fig
