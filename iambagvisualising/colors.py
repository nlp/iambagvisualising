#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Construct some colormap for visualising hierarchical datas.
"""

import matplotlib as mpl
import matplotlib.colors as mcolors

def colorscale(labels, nb_colors: int=256**3):
    """Transform a list os string into a list of float from 0 to 1"""
    return {labels[n]: n/(len(labels)-1) for n in range(len(labels))}

def colormap(labels, nb_colors: int=256**3, colormap='jet'):
    """Convert a list of labels into a RGB dictionary of colors."""
    colors = {labels[n]: mcolors.to_hex(
        mpl.colormaps[colormap]((2*n+1)/len(labels)/2))
              for n in range(len(labels))}
    return colors
