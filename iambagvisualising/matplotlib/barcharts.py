#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
barchart facilities in matplotlib
"""

import numpy as np
import matplotlib.pyplot as plt


def variable_size_histogram(
        datas,
        threshold: int = 1,
        bins: int = 100):

    def agglomerate_counts_bin_edges(counts, bin_edges, maxtot=1):
        """Combine the element of the counts and bin_edges if their values are smaller than maxtot. Return the reduced counts and bon_edges arrays.
        
        Combination are done on the minimal values, then agglomerated with the minimal neighbor.
        """
        def find_min(table, maxtot=10):
            minimum, position = maxtot, None
            for pos in range(len(table)):
                if table[pos][2] < minimum:
                    minimum = table[pos][2]
                    position = pos
            return minimum, position
        def construct_neighbors(table, position):
            neighbors = [table.pop(position)]
            positions = [pos for pos in range(len(table))
                         if (table[pos][0] == neighbors[0][1]
                             or table[pos][1] == neighbors[0][0])]
            for pos in sorted(positions)[::-1]:
                neighbors.append(table.pop(pos))
            return sorted(neighbors, key=lambda x: x[-1])
        def fuse_table(neighbors, table):
            fused = (min(neighbors[0][0], neighbors[1][0]),
                     max(neighbors[0][1], neighbors[1][1]),
                     neighbors[0][2] + neighbors[1][2])
            table.append(fused)
            table.extend(neighbors[2:])
            return table
        table = [(l, r, c) for l, r, c
                 in zip(bin_edges[:-1], bin_edges[1:], counts)]
        # find all minimal key:value of table that are smaller than maxtot
        minimum, position = find_min(table, maxtot=maxtot)
        # stop when there is no more value below maxtot
        while position is not None:
            neighbors = construct_neighbors(table, position)
            table = fuse_table(neighbors, table)
            minimum, position = find_min(table, maxtot=maxtot)
        table = sorted(table, key=lambda x: x[0])
        return (np.array([t[2] for t in table]),
                np.array([t[0] for t in table] + [table[-1][1]]))

    def string(x):
        """Convert numbers to string for the xticks."""
        return f"{x:.3f}" if isinstance(x, float) else str(x)

    fig, axs = plt.subplots(
        nrows=1, ncols=1,
        figsize=(12, 6),
        layout=None,
    )
    max_data = np.max(datas)
    quartiles = np.quantile(datas, [0.25, 0.5, 0.75])
    # histogram
    quantiles_max = np.quantile(datas, 0.95)
    mask = datas >= quantiles_max
    if np.sum(mask):
        datas[mask] = quantiles_max
        flag_truncated = True
    else:
        flag_truncated = False
    bins = len(mask) - np.sum(mask)
    counts, bin_edges = np.histogram(datas, bins=bins)
    counts, bin_edges = agglomerate_counts_bin_edges(
        counts, bin_edges, maxtot=threshold)
    # percentage
    counts = 100 * counts / np.sum(counts)
    widths = bin_edges[1:] - bin_edges[:-1]
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    # alternate colors
    colors = ['tab:blue', 'tab:cyan'] * (len(counts) // 2) + \
                ['tab:blue'] * (len(counts) % 2)
    if flag_truncated:
        colors[-1] = 'tab:orange'
    
    xticks = centers
    axs.set_xticks(xticks)
    axs.set_xticklabels(
        [string(x) for x in xticks[:-1]] + \
            [string(quantiles_max)+'-'+string(max_data)])
    axs.bar(centers, height=counts, width=widths, align='center', color=colors)
    
    # quartiles
    colors = ['tab:olive', 'tab:orange', 'tab:olive']
    styles = ['dashed', 'solid', 'dashed']
    axs.vlines(quartiles, 0, np.max(counts), colors=colors, linestyles=styles)
    return fig, axs


rng = np.random.default_rng()

datas = rng.integers(1, 45, size=150)
threshold = 6  # works only for 1 at the moment, otherwise overwrite datas

fig, axs = variable_size_histogram(datas, threshold=threshold)
