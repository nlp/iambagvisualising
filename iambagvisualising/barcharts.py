#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Construct some plotly graph associated to the display of bars.
"""
from typing import Dict, Sequence, Union
from collections import Counter

import plotly.graph_objects as go
from plotly.subplots import make_subplots


def bar_chart(x: Sequence[float], y: Sequence[float],
              labels: Union[Dict[str, str], None] = None,
              colors: Union[Dict[str, str], None] = None,
              ) -> go.Figure:
    """Construct a simple bar chart from x and y values (two sequences of same lengths).
    
    Parameters
    ----------
    
    x, y: sequences of floats
        The x and y values of the bars.
    labels: dict, optional
        Alternative labels to print while hovering the datas. Must be a dictionary of the form {x: alternative_x (a string)}
    colors: dict, optional
        The color associated to each bar. Must be a dictionary of the form {x: color_x (a plotly compatible color)}

    Returns
    -------
    fig : plotly.graph_objects.Figure object
        The plotly figure, that can be further updated or displayed.    
    """
    bar_data = {}
    if labels is not None:
        bar_data['labels'] = [labels.get(n, str()) for n in x]  
    if colors is not None:
        bar_data['colors'] = [colors.get(n, "#000000") for n in x]
        
    fig = go.Figure()
    fig.add_trace(
        go.Bar(
            x=x,
            y=y,
            texttemplate='%{y:.3s}',
            textposition='inside',
            textangle=0,
            textfont_color='white',
            text=bar_data.get('labels', x),
            hovertemplate='%{text}: %{y:.3s}',
            marker_color=bar_data.get('colors', x),
        ),
    )    
    return fig

def x_aligned_bar_charts(x: Sequence[float], y: Dict[str, Sequence[float]],
                         labels: Union[Dict[str, str], None] = None,
                         colors: Union[Dict[str, str], None] = None,
                         ) -> go.Figure:
    bar_data = {}
    if labels is not None:
        bar_data['labels'] = [labels.get(n, str()) for n in x]
    if colors is not None:
        bar_data['colors'] = [colors.get(n, "#000000") for n in x]

    fig = make_subplots(rows=len(y),
                        cols=1,
                        shared_xaxes=True,
                        vertical_spacing=0.001)
    for i, seq_name in enumerate(y.keys()):
        fig.add_trace(
            go.Bar(
                x=x,
                y=y[seq_name],
                texttemplate='%{y:.3s}',
                textposition='inside',
                textangle=0,
                textfont_color='white',
                text=bar_data.get('labels', x),
                hovertemplate='%{text}: %{y:.3s}',
                marker_color=bar_data.get('colors', x),
            ),
            row=i+1, col=1,
        )
        fig.update_yaxes(title_text=seq_name, row=i+1, col=1)
    fig.update_layout(showlegend=False)
    return fig


def box_plots(x: Sequence[str], y: Sequence[Sequence[float]],
              labels: Union[Dict[str, str], None] = {},
              colors: Union[Dict[str, str], None] = {},
              ) -> go.Figure:
    """Construct a simple collection of box plots in the form of an horizontal bar chart from x and y values (two sequences of same lengths). y is a sequence of sequences, each of the sub-sequence representing the statistical distribution of the inner box-plots. One box-plot per x value.
    
    Parameters
    ----------
    
    x, y: sequences of floats
        The x and y values of the bars.
    labels: dict, optional
        Alternative labels to print while hovering the datas. Must be a dictionary of the form {x: alternative_x (a string)}
    colors: dict, optional
        The color associated to each bar. Must be a dictionary of the form {x: color_x (a plotly compatible color)}

    Returns
    -------
    fig : plotly.graph_objects.Figure object
        The plotly figure, that can be further updated or displayed.    
    """
    if len(x) != len(y):
        raise ValueError("x and y must have same size")
    fig = go.Figure()
    for x_, y_ in zip(x, y):
        if sum(y_) > 0.01:
            fig.add_trace(
                go.Box(
                    name=x_,
                    y=y_,
                    boxpoints="all",
                    text=labels.get(x_, 'unknown'),
                    marker_color=colors.get(x_, "indianred"),
                ),
            )
    fig.update_traces(showlegend=False,
                      hoverinfo="x+y+text",
                      selector=dict(type='box'))
    return fig


def Pareto_chart(counter: Counter,
                 from_rank: int = 0,
                 to_rank: Union[int, None] = None,
                 logscale: bool = True,
                 labels: Union[Dict[str, str], None] = None,
                 ) -> go.Figure:
    """
    Construct the Pareto chart associated to a Counter object as a plotly Figure.

    A Pareto curve is a composite Figure with bars and curves, representing the rank of the count of unordered datas, and the cumulative associated percent

    Parameters
    ----------
    counter : collections.Counter object
        The counting statistics that will be represented.
    from_rank : int, optional
        The starting position of the visualisation. The default is 0.
    to_rank : int or None, optional
        The last position that will be visualised. The default is None, in which case all the series will be displayed.
    logscale : bool, optional
        Whether the y axis will be in log scale (for better visualisation of the power-law scaling). The default is True.
    labels : dict, optional
        Alternative labels to print while hovering the datas. Must be a dictionary of the form {x: alternative_x (a string)}

    Returns
    -------
    fig : plotly.graph_objects.Figure object
        The plotly figure, that can be further updated or displayed.

    """
    bar_data = dict(names=[],
                    values=[],
                    cumulpercent=[],
                    cumulvalue=[])

    total_values, total_value = sum(counter.values()), 0

    for name, value in counter.most_common()[from_rank:to_rank]:
        bar_data['names'].append(name)
        bar_data['values'].append(value)
        total_value += value
        bar_data['cumulpercent'].append(100 * total_value / total_values)
        bar_data['cumulvalue'].append(total_value)

    if labels is not None:
        bar_data['labels'] = [labels[n] for n in bar_data['names']]

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(
        go.Bar(
            x=bar_data['names'],
            y=bar_data['values'],
            marker_color="SteelBlue",
            texttemplate='%{y:.3s}',
            textposition='inside',
            textangle=0,
            textfont_color='white',
            text=bar_data.get('labels', bar_data['names']),
            hovertemplate='%{text}: %{y:.3s} occ.',
            yaxis='y1',
        ),
        secondary_y=False,
    )
    fig.add_trace(
        go.Scatter(
            x=bar_data['names'],
            y=bar_data['cumulpercent'],
            hovertemplate='%{y:.3s}% of cumulative datas',
            mode='lines+markers',
            marker=dict(
                size=10,
                symbol='circle',
                color='DarkOrange',
            ),
            yaxis='y2',
        ),
        secondary_y=True,
    )

    fig.update_layout(showlegend=False)
    fig.update_yaxes(title_text="occurences",
                     title_font_color="SteelBlue",
                     secondary_y=False)
    fig.update_yaxes(title_text="cumulative percent",
                     title_font_color="DarkOrange",
                     secondary_y=True)

    if logscale:
        fig.update_layout(yaxis1=dict(type="log"))

    return fig
