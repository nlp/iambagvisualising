#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Visualisation of the properties of the Bags in iambagging companion library.

Run this app with `python bagdash.py` and
visit http://127.0.0.1:6088/ in your web browser.
"""
from dash import Dash, dcc, html, Input, Output, dash_table
from plotly.graph_objects import Figure

from collections import Counter
from typing import Tuple

import numpy as np
import pandas as pd

from iambagging import BagOfWords, CoOccurrence, JointFrequency
from iambagging import transformation as transfo
from iambagging.associations import disproportionality as assdisp

from iambagvisualising import multivariate
from iambagvisualising import barcharts
from iambagvisualising import scatter
from iambagvisualising.colors import colormap, colorscale


bowfilename = "data/dash_visualisation"

app = Dash(__name__)


#################################
# DATA SELECTION AND GENERATION #
#################################

bow_filename = 'test/random_bow.npz'
bow = BagOfWords(verbose=False)
bow.load(bow_filename)
bowbool = bow.make_boolean()

# number of occurrence of each vocab
counter_vocab = Counter({k: v for k, v in zip(bow.vocabularray, bow.vocount)})
# number of documents into which the vocab appears
counter_document = Counter({k: v for k, v in zip(bow.vocabularray, bowbool.docount)})
# number of vocabs per document
counter_document_length = Counter(bow.docount)
# number of unique vocabs per document
counter_document_length_unique_vocab = Counter(bowbool.docount)
# labels construction
labels = {vocab: 'label of ' + vocab for vocab in bow.vocabularray}

# creation of the DataTable
data = [{'vocab': vocab,
         'count_vocab': counter_vocab[vocab],
         'label': 'label of ' + vocab,
         'count_document': counter_document[vocab],
         'id': vocab,}
        for vocab in bow.vocabularray]
data = pd.DataFrame(data)

jft = JointFrequency((bow))
# because numbers are too large for some statistics, if they are calculated from int
jft.bag.data = jft.bag.data.astype(float)

args = jft.explode_for_calculation()
_fm = jft._construct_assstat(assdisp._fowlkes_mallows(*args))
_jaccard = jft._construct_assstat(assdisp._jaccard(*args))
_odds = jft._construct_assstat(assdisp._odds_ratio(*args))
_phi = jft._construct_assstat(assdisp._phi(*args))
_rd = jft._construct_assstat(assdisp._risk_difference(*args))
_f1 = jft._construct_assstat(assdisp._f1(*args))
association_statistics = [jft, _f1, _fm, _jaccard, _odds, _phi, _rd]
association_statistics_names = [
    "Cooccurrence",
    "F-score",
    "Fowlkes–Mallows_index",
    "Jaccard_index",
    "Odds_ratio",
    "Phi_coefficient",
    "Risk_difference",
]
association_statistics_short_descriptions = [
    "Simple counting of co-occurrences ; \[0,+oo\[",
    "Harmonic mean of precision and recall ; \[0,1\]",
    "Geometric mean of precision and recall ; \[0,1\]",
    "Intersection over union of datas ; \[0,1\]",
    "Ratio of the odds of A with B over the odds of A without B ; \[0, +oo\[",
    "Pearson coefficient for binary variables ; \[-1,1\]",
    "Difference between exposed and unexposed groups ; \[-1,1\]",
]

label_colormap = colormap(sorted(bow.vocabularray))

# app.layout is defined at the bottom of the construction of all dynamic elements

####################
# DYNAMIC ELEMENTS #
####################

# COUNTING TABLE

table_title = "Counting of vocabulary"
table_markdown_legend = """
__Filter the table__

For more details, [see the official documentation](https://dash.plotly.com/datatable/filtering).
"""
table = html.Div([
    html.H2(table_title),
    dash_table.DataTable(
        id='table',
        # editable=True,
        filter_action="native",
        sort_action="native",
        # sort_mode="multi",
        # column_selectable="single",
        # row_selectable="multi",
        # row_deletable=True,
        # selected_columns=[],
        # selected_rows=[],
        page_action="native",
        page_current=0,
        page_size=12,
        data=data.to_dict('records'),
        columns=[{"name": i, "id": i} for i in data.columns],
    ),
    dcc.Markdown(table_markdown_legend),
])

# RANKING FIGURE

ranking_title = "Ranking of the vocabulary"
ranking_legend_markdown = """
__[Pareto chart](https://en.wikipedia.org/wiki/Pareto_chart) of the ranking__

The occurrence of the vocabulary over the entire catalog, ranked by occurrence. The blue bars represent the occurrence number on the left (log) scale. The hexagonal diamonds represent the cumulative percent of the total number of occurrence at each rank, in percent on the right axis.
"""
size = 10
intermediary_truncation = int(len(counter_vocab) * size / 100)
cumulative = np.cumsum([_[1] for _ in counter_vocab.most_common()])
ranking_first_legend_markdown = f"""
Same representation as for the above Pareto chart, only for the first {size}% of the ranked datas. The first {size}% represent {100 * cumulative[intermediary_truncation] / cumulative[-1]:.2f}% of the total datas.
"""
ranking_last_legend_markdown = f"""
Same representation as for the above Pareto chart, only for the last {100-size}% of the ranked datas. The last {100-size}% represent {100 * (cumulative[-1] - cumulative[intermediary_truncation]) / cumulative[-1]:.2f}% of the total datas.
"""

stats_vocabulary = html.Div([
    html.H2(ranking_title),
    dcc.Graph(
        id="vocabulary-ranking",
        figure=barcharts.Pareto_chart(
            counter=counter_vocab,
            labels=labels,
        ),
    ),
    dcc.Markdown(ranking_legend_markdown),
    dcc.Graph(
        id="ranking-first-ranks",
        figure=barcharts.Pareto_chart(
            counter=counter_vocab,
            labels=labels,
            to_rank=intermediary_truncation,
        ),
    ),
    dcc.Markdown(ranking_first_legend_markdown),
    dcc.Graph(
        id="ranking-last-ranks",
        figure=barcharts.Pareto_chart(
            counter=counter_vocab,
            labels=labels,
            from_rank=intermediary_truncation,
        ),
    ),
    dcc.Markdown(ranking_last_legend_markdown),
])

# DOCUMENT STATISTICS

x, y = [], []
for k in sorted(counter_document_length.keys()):
    x.append(k)
    y.append(counter_document_length[k])

document_title = "Statistics over the document"
document_legend_markdown = """
__Distribution of the document lengths__

Number of tokens per document. Each bar represent the number of times the length has been found in the entire catalog. The length possibilities are represented along the horizontal axis.
"""
fig = barcharts.bar_chart(x, y)
fig.update_layout(
    yaxis_title="occurences",
    xaxis_title="length of document (in tokens)")
stats_document = html.Div([
    html.H2(document_title),
    dcc.Graph(
        id="document-length",
        figure=fig,
    ),
    dcc.Markdown(document_legend_markdown),
])

# NEW VOCAB PER DOCUMENT

nb_shuffling = 120
nvocab_perdoc = np.zeros(shape=(nb_shuffling, bow.shape[0]), dtype=int)
for i in range(nb_shuffling):
    nvocab_perdoc[i] = bow.new_tokens_per_document(shuffle=True)
x = np.arange(bow.shape[0] + 1)
y = np.concatenate([np.zeros(shape=(1,)),
                    np.cumsum(np.mean(nvocab_perdoc, axis=0))])
error = np.concatenate([np.zeros(shape=(1,)),
                        np.std(nvocab_perdoc, axis=0)])

document_new_vocab_legend_markdown = """
__Size of the vocabulary per number of opened documents__

Size of the vocabulary with respect to the number of opened documents. The catalog is shuffled several times to change the order of appearance of the new vocabular while parsing the documents. The mean and the standard deviation of this shuffling is displayed as point and error bars in the figure.
"""
fig = scatter.error_plot(x, y, error)
fig.update_layout(
    yaxis_title="size of vocabulary",
    xaxis_title="number of opened documents")
stats_document.children.append(dcc.Graph(
    id="new-vocab-per-document",
    figure=fig))
stats_document.children.append(
    dcc.Markdown(document_new_vocab_legend_markdown))


# ASSOCIATION STATISTICS

association_title = "Association statistics"

stats_multivariate = html.Div([
    html.H2(association_title),
    html.H3("Parallel coordinates of the associations"),
    dcc.Graph(id="parallel-coordinates"),
    dcc.Markdown(id="parallel-coordinates-legend"),
    html.H3("All selected statistics"),
    dcc.Graph(id="selected-statistics"),
    dcc.Markdown(id="selected-statistics-legend"),
])

#############
# CALLBACKS #
#############


@app.callback(
    Output('parallel-coordinates', 'figure'),
    Output('parallel-coordinates-legend', 'children'),
    Output('selected-statistics', 'figure'),
    Output('selected-statistics-legend', 'children'),
    Input('table', 'derived_virtual_data'),
    Input('table', 'selected_cells'),
    prevent_initial_call=True,
)
def code2statistics(
        selected_datas,  # Selected data in DataTable
        active_cells,  # Active cell in DataTable
) -> Tuple[Figure]:
    """Construct the association statistics associated to the code selected in the table."""
    if active_cells:
        vocabs = [d['row_id'] for d in active_cells]
    else:
        vocabs = [d['id'] for d in selected_datas]
    vocab = vocabs[0]
    return (*construct_parallel_chart_from_vocab(vocab),
            *construct_selected_stats_from_vocab(vocab))


def construct_parallel_chart_from_vocab(vocab: str) -> Figure:
    """Construct the parallel plot of all scores for all codes at once."""
    scores = [{_[0]:_[1] for _ in getattr(x, 'ntop')(vocab)}
              for x in association_statistics]
    df = pd.DataFrame(scores)
    df.index = association_statistics_names
    df = df.T
    color_scale = [label_colormap.get(c, 0) for c in df.index]
    fig = multivariate.parallel_coordinates(
        df.T,
        color_scale=color_scale,
        legend_title="Vocabulary")
    fig.update_layout(title=vocab)
    legend = f"Parallel coordinate plots of all statistics for '{vocab}'"
    return fig, legend


def construct_selected_stats_from_vocab(vocab: str) -> Figure:
    """Construct the bar charts for all codes, one bar chart per statistics."""
    ntops = [getattr(x, 'ntop')(vocab) for x in association_statistics]
    bar_statistics = {}
    for statname, ntop in zip(association_statistics_names, ntops):
        x, y = [], []
        for x_, y_ in sorted(ntop, key=lambda x: x[0]):
            x.append(x_)
            y.append(y_)
        bar_statistics[statname] = y
    fig = barcharts.x_aligned_bar_charts(x, bar_statistics)
    fig.update_layout(title=f"Association statistics for {vocab}")
    fig.update_layout(height=150*len(statname))
    legend = f"Association statistics plots for '{vocab}'"
    return fig, legend


##########
# LAYOUT #
##########

title = html.H1("Exemple of BagOfWords visualisation")

app.layout = html.Div(children=[
    title,
    stats_vocabulary,
    table,
    stats_document,
    stats_multivariate,
    # stats_association,
])


if __name__ == '__main__':
    app.run_server(debug=True,
                   dev_tools_hot_reload=True,
                   host='0.0.0.0',
                   port="6088",
                   )
