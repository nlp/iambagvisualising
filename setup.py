#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt', 'r') as file:
    requirements_ = [line.strip() for line in file.readlines()
                     if not line.startswith('#')]

setuptools.setup(
    name="iambagvisualising",
    version="0.2.5",
    author="IAM-CHU-Bordeaux-France",
    author_email="no_email@please.org",
    description="Facilities to visualise bag of words and co. Companion project of iamBagging",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/nlp/iambagvisualising",
    packages=setuptools.find_packages(),
    install_requires=requirements_,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Text Processing",
        "Intended Audience :: Information Technology",
        "Intended Audience :: Science/Research",
    ],
    python_requires='>=3.6',
)
